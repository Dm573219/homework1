
#nginx version: nginx/1.19.3
# Be sure to be in the working directory i.e if downloaded this and put it in the Desktop // cd Desktop Then run commands. 
docker run -d -p 10000:80 -v $PWD/part1/web1:/usr/share/nginx/html --name Web1 nginx:1.19.3
docker run -d -p 10001:80 -v $PWD/part1/web2:/usr/share/nginx/html --name Web2 nginx:1.19.3
docker run -it -v $PWD/part1/db1:/data/db --name db1 -d mongo:4.4
docker run -it -v $PWD/part1/db2:/data/db --name db2 -d mongo:4.4
docker ps 
echo "All processes started."
